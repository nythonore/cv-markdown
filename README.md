# My CV

#### Hello 👋, I'm Honore <br /> I help companies [design, develop, launch] world class digital products.
Frontend Developer [@IshemaHub](https://ishemahub.co.rw/)

Highly motivated and self-taught in software development and programming principles. Profecient <br/> in a variety of platforms, languages with an intate to learn and master new technologies.

## About Me
I have experiences in development & design work. I prefer to keep learning, continue challenging myself, and do interesting things that matter. My abundant energy fuels me in the pursuit of many interests, hobbies, areas of study and artistic endeavors. I’m a fast learner, able to pick up new skills and juggle different projects and roles with relative ease.

I’m a people-person with deep emotions and empathy, a natural storyteller. I’m able to inspire and am at my best when I’m sharing my creative expressions with others.

## Skills
- HTML & CSS | Bootstrap
- Javascript (React JS & React Native)
- Python (Django & Fast API)
- Php (Laravel)
- Datebase (Mysql & PostgreSQL)
- Devops (Git & Docker)

<br/>

### Feel free to reach out for <br/> collaborations or just a friendly <br/> hello niyitegekah@gmail.com
